'Sandboxing analysis of files in a folder using McAfee ATD'


Uso: atddir.py [-h] -u USER -p PASSWORD -atd ATD_IP -path path_to_folder_to_anlyze [-vm profile_id] [-q path_to_quarantine_folder] [-force] [-delta seconds] [-monitor]
    

MANDATORY PARAMETERS

	AUTHENTICATION PARAMETERS

	-u: Username to connect to McAfee ATD
	-p: Password used to authenticate user
	-atd: IP address, where ATD is located

	-path: Path to the folder to analyze

OPTIONAL PARAMETERS

-vm: Profile identificator, if not set the profile associated to the user will be used.

-q: Path to the quarantine folder, if enable all files with a severity higher than 2 (suspicious) will be moved to the quarantine folder.

-force: All files analyzed are stored in a SQLlite Database created in the same path where the script resides. If during a second analisys the same file (HASH - SHA256) 
is seen the system will notify that the file was already analyized will present the severity calculated and it won't analyze it again. This parameter force the system
to reanalyze the file.

-delta: Same as -force, but in this case the process allows you to setup an amount of seconds, so the files will be re-analyzed when this threshold will be reached.

-monitor: It allows the system to work in a not ending loop, monitoring constantly the folder indicated in path

The script shows some messages in the console as well as in a log file called atd_log.log